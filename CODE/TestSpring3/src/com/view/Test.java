package com.view;




import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.bean.TeacherBean;
import com.service.Course;
import com.service.CourseImpl;

public class Test {
	
	public static void main (String args[]){
//		TeacherBean bean =new TeacherBean();
//		bean.setName("��");
//		Course course=new CourseImpl("java");
//		course.process(bean);
		try{
	 ApplicationContext context =new ClassPathXmlApplicationContext("applicationContext.xml");
	 TeacherBean teacherbean=(TeacherBean) context.getBean("teacher");
	 Course course=(CourseImpl) context.getBean("proxy");
	 course.process(teacherbean);
		}catch(Exception ex){
			
			ex.printStackTrace();
		}
	 
	
	}

}
