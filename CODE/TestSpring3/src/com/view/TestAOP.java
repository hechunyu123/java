package com.view;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.bean.StudentBean;
import com.bean.TeacherBean;
import com.service.StudentService;
import com.service.TeacherService;

public class TestAOP {
	public static void main (String args[]){
		try {
			
//			ApplicationContext context=new ClassPathXmlApplicationContext("applicationContext.xml");
//			TeacherBean teacher=(TeacherBean) context.getBean("teacher");
//			TeacherService  teacherservice=(TeacherService) context.getBean("teacherservice");
//			teacherservice.AddTeacher(teacher);
//			teacherservice.deleteTeacher(teacher);
//			
//			
//			StudentBean student=(StudentBean) context.getBean("studentbean");
//			StudentService  studentservice=(StudentService) context.getBean("studentservice");
//			studentservice.Addstudent(student);
//			studentservice.deleteStudent(student);

			ApplicationContext context=new ClassPathXmlApplicationContext("applicationContext.xml");
		
			TeacherBean teacher=(TeacherBean) context.getBean("teacher");
			TeacherService  teacherservice=(TeacherService) context.getBean("advice");
			teacherservice.AddTeacher(teacher);
			teacherservice.deleteTeacher(teacher);
			
//			StudentBean student=(StudentBean) context.getBean("studentbean");
//			StudentService  studentservice=(StudentService) context.getBean("advice");
//			studentservice.Addstudent(student);
//			studentservice.deleteStudent(student);
			
		}catch (Exception ex){
			
			ex.printStackTrace();
		}
		
		
	}

}
