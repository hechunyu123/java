package com.dao;

import java.util.List;

import com.bean.StudentBean;

public interface  StudentDao {
	public int  Addstudent(StudentBean student);
	public int updateStudent(StudentBean student);
	public int deleteStudent(StudentBean student);
	public StudentBean selectstudentByname(String name);
	public List<StudentBean> selectAll ();
	
	
}
