package com.dao.impl;

import java.util.List;

import com.bean.StudentBean;
import com.dao.StudentDao;

public class StudentDaoImpl implements StudentDao{


	public int Addstudent(StudentBean student) {
		System.out.println("添加学生");
		return 0;
	}

	@Override
	public int updateStudent(StudentBean student) {
		System.out.println("修改学生");
		return 0;
	}

	@Override
	public int deleteStudent(StudentBean student) {
		System.out.println("删除学生");
		return 0;
	}

	@Override
	public StudentBean selectstudentByname(String name) {
		System.out.println("通过名字查询学生");
		return null;
	}

	@Override
	public List<StudentBean> selectAll() {
		System.out.println("查询所有学生");
		return null;
	}
	

}
