package com.dao;

import java.util.List;

import com.bean.TeacherBean;

public interface TeacherDao {
	public int AddTeacher(TeacherBean bean);
	public int updateTeacher(TeacherBean bean);
	public int deleteTeacher(TeacherBean bean);
	public TeacherBean selectByName (String name);
	public List<TeacherBean > selectAll();

}
