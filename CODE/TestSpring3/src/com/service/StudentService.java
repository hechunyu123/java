package com.service;

import com.bean.StudentBean;
import com.dao.StudentDao;
import com.dao.impl.StudentDaoImpl;

public class StudentService {
	private StudentDao dao = new StudentDaoImpl();

	public StudentDao getDao() {
		return dao;
	}

	public void setDao(StudentDao dao) {
		this.dao = dao;
	}

	public int Addstudent(StudentBean student) {

		return dao.Addstudent(student);
	}

	public int updateStudent(StudentBean student) {

		return dao.updateStudent(student);
	}

	public int deleteStudent(StudentBean student) {

		return dao.deleteStudent(student);
	}

	public StudentBean selectstudentByname(String name) {

		return dao.selectstudentByname(name);
	}

}
