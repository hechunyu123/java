package com.service;

import java.lang.reflect.Method;

import org.springframework.aop.MethodBeforeAdvice;

import com.bean.TeacherBean;

public class CourseBeforeAdvice implements MethodBeforeAdvice{
	public void before(Method arg0, Object[] args, Object obj)
			throws Throwable {
		// TODO Auto-generated method stub

	      TeacherBean teacher = (TeacherBean) args[0];
			System.out.println(teacher.getName()+"老师");
			System.out.println("你有一门课程需要准备");
			Course course = (Course) obj;
			System.out.println("课程名称是"+course.getName());
			System.out.println("---------------------------");
	      
	      
		
	}

}
