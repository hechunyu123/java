package com.service;

import java.util.List;

import com.bean.TeacherBean;
import com.dao.TeacherDao;
import com.dao.impl.TeacherDaoImpl;

public class TeacherService {
	private TeacherDao dao=new TeacherDaoImpl();
	public TeacherDao getDao() {
		return dao;
	}


	public void setDao(TeacherDao dao) {
		this.dao = dao;
	}


	public int AddTeacher(TeacherBean bean) {
		
		return dao.AddTeacher(bean);
	}


	public int updateTeacher(TeacherBean bean) {
		
		return dao.updateTeacher(bean);
	}


	public int deleteTeacher(TeacherBean bean) {
	
		return dao.deleteTeacher(bean);
	}


	public TeacherBean selectByName(String name) {
		
		return dao.selectByName(name);
	}


	public List<TeacherBean> selectAll() {
	
		return dao.selectAll();
	}

}
