package com.service;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

import com.util.Log;

public class LogAroundAdvice implements MethodInterceptor {

	public Object invoke(MethodInvocation method) throws Throwable {
		
		Log log =new Log();
		log.process1();
		//环绕通知在之前执行和之后执行
		
		Object returnvalue=method.proceed();
		
		return returnvalue;
	}

}
