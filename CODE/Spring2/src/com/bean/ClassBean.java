package com.bean;

import java.util.List;
import java.util.Map;

public class ClassBean {
	private String name ;
	private List<StudentBean> students ;
	private Map <String ,StudentBean> stu;
	public String getName() {
		return name;
	}
	public Map<String, StudentBean> getStu() {
		return stu;
	}
	public void setStu(Map<String, StudentBean> stu) {
		this.stu = stu;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<StudentBean> getStudents() {
		return students;
	}
	public void setStudents(List<StudentBean> students) {
		this.students = students;
	}
	
	

}
