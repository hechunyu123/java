package com.bean;

public class StudentBean {
	private String name;
	private int age;
	private ClassBean classbean;

	public ClassBean getClassbean() {
		return classbean;
	}

	public void setClassbean(ClassBean classbean) {
		this.classbean = classbean;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String toString() {

		return "姓名" + name + "\n年龄" + age;
	}

	public void chushi() {
		System.out.println("初始化");
	}

	public void xiaohui() {
		System.out.println("销毁");
	}
}
