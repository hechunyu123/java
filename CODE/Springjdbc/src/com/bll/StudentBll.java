package com.bll;

import java.util.List;

import com.bean.StudentBean;
import com.dao.StudentDao;

public class StudentBll {
	// bll����dao
	private StudentDao dao = new StudentDao();

	public StudentDao getDao() {
		return dao;
	}

	public void setDao(StudentDao dao) {
		this.dao = dao;
	}

	public int addStudent(StudentBean bean) {
		return dao.addStudent(bean);

	}
	public List<StudentBean> selectAll(){
		return dao.selectAll();
	}
	public int updatestudent(StudentBean bean){
		return dao.updatestudent(bean);
	}

}
