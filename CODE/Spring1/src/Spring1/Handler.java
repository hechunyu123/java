package Spring1;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class Handler implements InvocationHandler  {

	private Object delegate;
    public Object bind(Object delegate) {
         this.delegate = delegate;
         return Proxy.newProxyInstance(delegate.getClass().getClassLoader(),                          
                          delegate.getClass().getInterfaces(), this);
    }
    public Object invoke(Object proxy, Method method, Object[] args){
         Object result = null;
         System.out.println("----------方法执行之前-----------");
         try {
			result = method.invoke(delegate, args);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
         System.out.println("----------方法执行之后-----------");
         return result;
    }

}
