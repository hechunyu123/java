package Spring1;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

import com.bean.StudentBean;

public class Test {
	// 实现动态调用某一类的某一方法
	// className 类名，methodname方法名，args参数
	// Object返回值
	public static Object myInvoke(String className, String methodname,
			Object args[]) {
		Object results = null;
		try {
			Class c = Class.forName(className);
			Method method = null;
			// 所有方法
			for (int i = 0; i < c.getMethods().length; i++) {
				// 某个方法
				method = c.getMethods()[i];
				if (methodname.equals(method.getName())) {
					results = method.invoke(c.newInstance(), args);
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return results;
	}

	public static void main(String args[]) {

		// IHello h=new IHelloImpl();
		// h.hello();
		Handler handler = new Handler();
		// 代理类绑定实现类
		IHello Ihello = (IHello) handler.bind(new IHelloImpl());
		Ihello.hello();

		// Object obj=Test.myInvoke("com.bean.StudentBean", "setAge", new
		// Object[]{90});
		// Object obj1=Test.myInvoke("com.bean.StudentBean", "getAge", null);
		// System.out.println(obj1);
		// StudentBean bean =new StudentBean("zs",19);
		// try {
		// Class c =Class.forName("com.bean.StudentBean");
		// // Object obj=c.newInstance();
		// Class [] parameterTypes ={String.class,int.class};
		// Constructor constructor=c.getConstructor(parameterTypes);
		// Object value []={"lisi",20};
		// Object obj=constructor.newInstance(value);
		// System.out.println(obj);
		// //动态调用方法invoke
		// Class a []={int.class};
		// Method setAge=c.getMethod("setAge", a);
		// Object value1[]={30};
		// setAge.invoke(obj, value1);
		// System.out.println(obj);
		// } catch (Exception e) {
		//
		// e.printStackTrace();
		// }
		//

	}

}
