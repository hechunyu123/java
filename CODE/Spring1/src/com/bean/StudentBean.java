package com.bean;

public class StudentBean {
	private String id ;
	private String name ;
	private String sex;
	private int age;
	public StudentBean(){
		
		System.out.println("无参的构造方法被调用");
	}
	public StudentBean(String name,int age){
		this.name=name;
		this.age=age;
		System.out.println("有参的构造方法被调用");
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
		
	}
	public String  toString (){
		
		
		return  name+"\n"+age;
	}

}
